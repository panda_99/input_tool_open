# input-tool

#### 介绍
一个按键赋值工具，支持批量点击/批量与动态输入提交

#### 软件架构
UI: tkinter + ctypes
按键：win32gui + pyautogui + keyboard
主代码：input-tool.py


#### 打包exe教程

运行builder.py即可, 运行前需要`pip install pyinstaller`安装Pyinstaller


#### 虚拟环境优化打包
- 查询依赖：`pip install pipreqs`安装pipreqs， 切换到目录运行`pipreqs ./ --encoding=utf8` 生成requirements.txt文件，内容是项目依赖
- 虚拟环境：`pip install pipenv`安装，`pip install pipenv`进入（`exit`退出），虚拟环境安装打包工具`pip install pyinstaller`，并安装pipreqs查询到的依赖，进入ui目录执行`Pyinstaller -F -w -i fav.ico input-tool.py`打包


#### 界面示意图

![xx](./ui/desc_images/desc1.png)

#### 界面示意图（新版）

![xx](./ui/desc_images/desc2.png)
![xx](./ui/desc_images/desc3.png)

