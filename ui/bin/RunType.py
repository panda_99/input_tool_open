from enum import Enum


class RunType(Enum):
    Stop = 0
    Input = 1
    InputRandom = 2
    InputSort = 3
    click = 4