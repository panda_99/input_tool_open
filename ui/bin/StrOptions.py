from os import getcwd, path
from random import choice
from tkinter import StringVar, filedialog, END


class StrOptions:
    def __init__(self):
        self.name = "./text_list.text"
        self.options = []

        self.index = 0
        self.ingOption = StringVar()

        self.readRows(self.name)

    def save(self, items):
        file = open(self.name, 'w+', encoding='utf8')
        for i in range(len(items)):
            item = items[i]
            if item == "":
                continue
            if i < len(items) - 1:
                file.write(item + '\n')
            else:
                file.write(item)
        file.close()
        self.options = items

    def readRows(self, pathX):
        if not path.exists(pathX):
            return
        file = open(pathX, 'r+', encoding='utf8')
        self.options = file.readlines()
        for data in self.options:
            if data == '':
                self.options.remove("")
        file.close()

    def randomText(self):
        if len(self.options) == 0:
            return ""
        self.ingOption.set(choice(self.options))
        return self.ingOption.get()

    def sortNextText(self):
        length = len(self.options)
        if length == 0:
            return ""
        self.index = (self.index + 1) % length
        self.ingOption.set(self.options[self.index])
        return self.ingOption.get()

    # 加载到组件
    def loadToTextComp(self, textComp):
        textComp.delete('1.0', END)
        for item in self.options:
            if item == "":
                continue
            textComp.insert("end", item)

    def saveByTextComp(self, textComp):
        context = textComp.get(1.0, "end")
        options = context.split("\n")
        options.pop()
        self.save(options)

    def loadFileOptionsToTextComp(self, textComp):
        # 打开文件选择器，选择文本文件
        file_path = filedialog.askopenfilename(initialdir=getcwd(), filetypes=[("Text files", "*.txt")])
        if file_path == "":
            return
        self.readRows(file_path)
        self.loadToTextComp(textComp)
