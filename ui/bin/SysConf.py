from json import loads, dumps
from tkinter import StringVar
from os import path

"""
  {
    "input": {
      "total": 1,
      "text": "1",
      "ixy": "1:1",
      "sxy": "0:0",
      "tm": 1200
    },
    "click": {
      "total": 10000,
      "cxy": "1:1"
    }
  }
"""


class SysConf:

    def __init__(self):

        self.confName = "./conf.json"

        self.clickTotal = StringVar(value=1)  # 赞数量
        self.clickXY = StringVar(value="1:1")  # 点赞坐标
        self.clickTm = StringVar(value="100")  # 点赞间隔

        self.inputText = StringVar(value="1")  # 输入文本
        self.inputTotal = StringVar(value=1)  # 输入次数
        self.inputXY = StringVar(value="1:1")  # 输入坐标
        self.submitXY = StringVar(value="0:0")  # 提交坐标
        self.submitTm = StringVar(value="100")  # 提交间隔
        self.theme = StringVar(value="#e9ffe2")  # 主题

        self.readConf()

        # 文本列表
        self.options = StringVar()

    # 读取配置文件
    def readConf(self):
        # 读取配置
        if not path.exists(self.confName):
            return
        fileConf = open(self.confName, "r+", encoding='utf8')
        lines = fileConf.read()
        fileConf.close()
        if lines == "":
            return
        jData = loads(lines)

        if len(jData) > 0:
            # 输入配置
            inputX = jData.get("input")
            if inputX is not None:

                inputX_total = inputX.get("total")
                if inputX_total is not None:
                    self.inputTotal.set(inputX_total)

                inputX_text = inputX.get("text")
                if inputX_text is not None:
                    self.inputText.set(inputX_text)

                inputX_ixy = inputX.get("ixy")
                if inputX_ixy is not None:
                    self.inputXY.set(inputX_ixy)

                inputX_sxy = inputX.get("sxy")
                if inputX_sxy is not None:
                    self.submitXY.set(inputX_sxy)

                inputX_tm = inputX.get("tm")
                if inputX_tm is not None:
                    self.submitTm.set(inputX_tm)

            # 点击配置
            clickX = jData.get("click")
            if clickX is not None:

                clickX_total = clickX.get("total")
                if clickX_total is not None:
                    self.clickTotal.set(clickX_total)

                clickX_cxy = clickX.get("cxy")
                if clickX_cxy is not None:
                    self.clickXY.set(clickX_cxy)

                clickX_tm = clickX.get("tm")
                if clickX_tm is not None:
                    self.clickTm.set(clickX_tm)

            themeStr = jData.get("theme")
            if themeStr is not None:
                self.theme.set(themeStr)

    # 保存配置文件
    def saveConf(self):
        clickTotal = self.clickTotal.get()  # 赞数量
        clickXY = self.clickXY.get()  # 点赞坐标
        clickTm = self.clickTm.get()

        inputText = self.inputText.get()  # 输入文本
        inputTotal = self.inputTotal.get()  # 输入次数
        inputXY = self.inputXY.get()  # 输入坐标
        submitXY = self.submitXY.get()  # 提交坐标
        submitTm = self.submitTm.get()  # 提交间隔

        jsonStr = dumps({
            "input": {
                "total": inputTotal,
                "text": inputText,
                "ixy": inputXY,
                "sxy": submitXY,
                "tm": submitTm
            },
            "click": {
                "total": clickTotal,
                "cxy": clickXY,
                "tm": clickTm
            },
            "theme": self.theme.get()
        }, ensure_ascii=False)
        # 打开/创建文件
        f = open(self.confName, "w+", encoding='utf8')
        f.write(jsonStr)
        f.close()

    def get_xy_click(self):
        sx, sy = self.clickXY.get().split(":")
        return int(sx), int(sy)

    def get_xy_input(self):
        sx, sy = self.inputXY.get().split(":")
        return int(sx), int(sy)

    def get_xy_input_submit(self):
        sx, sy = self.submitXY.get().split(":")
        return int(sx), int(sy)

