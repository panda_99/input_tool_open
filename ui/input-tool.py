import ctypes  # 系统api
import os
from tkinter import Label, Entry, Text, Tk, Menu, colorchooser, StringVar

import win32gui

from ui.bin.RunType import RunType
from ui.bin.StrOptions import StrOptions
from ui.bin.SysConf import SysConf

ctypes.OleDLL('shcore').SetProcessDpiAwareness(1)

import threading  # 多线程
import pyautogui  # 鼠标
import keyboard  # 按键
from time import sleep


class Main(Tk):
    def __init__(self, master=None, *args, **kwargs):

        super().__init__(master, *args, **kwargs)
        self.start_left = self.winfo_screenwidth() // 4
        self.start_top = self.winfo_screenheight() // 4
        self.width = 640  # self.winfo_screenwidth() // 2
        self.height = 520  # self.winfo_screenheight() // 2
        self.all_title = "[模拟工具]"
        self.title(self.all_title)
        self.attributes("-alpha", 1)
        self.geometry("{}x{}+{}+{}".format(self.width, self.height, self.start_left, self.start_top))
        self.resizable(False, False)
        self.configure(bg="#e9ffe2")
        self.attributes("-alpha", 0.9)
        self.update()

        # 对象
        self.confO = SysConf()
        self.randomO = StrOptions()

        self.label_comps = []
        self.input_comps = []
        self.menu_comps = []

        # 局部参数
        self.run_type = RunType.Stop

        ''' one：一个重复 / sort: 顺序 / random： 随机 '''
        self.readType = "sort"
        self.optionsText = None

        # 创建菜单
        self.create_menu()

        # 加载动态数据
        self.update_coordinate_show()

        # 加载组件
        self.showInputList()

        # 读入options
        self.randomO.loadToTextComp(self.optionsText)

        # 主题
        self.config(background=self.confO.theme.get())
        self.apply_theme()



    def create_menu(self):
        menubar = Menu()

        def select_xy(value):
            point = pyautogui.position()
            value.set("{}:{}".format(point.x, point.y))

        # 评论
        comment_menu = Menu(menubar, tearoff=False)
        keyboard.add_hotkey('alt+i', lambda: select_xy(self.confO.inputXY))
        comment_menu.add_command(label="选定输入坐标", accelerator="Alt + I", command=lambda: select_xy(self.confO.inputXY))
        keyboard.add_hotkey('alt+j', lambda: select_xy(self.confO.submitXY))
        comment_menu.add_command(label="选定提交坐标", accelerator="Alt + J", command=lambda: select_xy(self.confO.submitXY))
        comment_menu.add_separator()
        keyboard.add_hotkey('alt+o', self.input_start)
        comment_menu.add_command(label="固定开始", accelerator="Alt + O", command=self.input_start)
        keyboard.add_hotkey('alt+p', self.input_sort_start)
        comment_menu.add_command(label="有序开始", accelerator="Alt + P", command=self.input_start)
        keyboard.add_hotkey('alt+q', self.input_random_start)
        comment_menu.add_command(label="随机开始", accelerator="Alt + Q", command=self.input_random_start)
        comment_menu.add_separator()
        keyboard.add_hotkey('alt+y', self.input_stop)
        comment_menu.add_command(label="停止", accelerator="Alt + Y", command=self.input_stop)
        comment_menu.add_separator()
        keyboard.add_hotkey('alt+f', lambda: self.randomO.loadFileOptionsToTextComp(self.optionsText))
        comment_menu.add_command(label="文本表切换", accelerator="Alt + F",
                                 command=lambda: self.randomO.loadFileOptionsToTextComp(self.optionsText))
        keyboard.add_hotkey('alt+g', lambda: self.randomO.saveByTextComp(self.optionsText))
        comment_menu.add_command(label="文本表保存", accelerator="Alt + G",
                                 command=lambda: self.randomO.saveByTextComp(self.optionsText))
        menubar.add_cascade(label="评论", menu=comment_menu)

        # 点赞
        click_menu = Menu(menubar, tearoff=False)
        keyboard.add_hotkey('alt+k', lambda: select_xy(self.confO.clickXY))
        click_menu.add_command(label="选定点击坐标", accelerator="Alt + K", command=lambda: select_xy(self.confO.clickXY))
        click_menu.add_separator()
        keyboard.add_hotkey('alt+d', self.click_start)
        click_menu.add_command(label="开始", accelerator="Alt + D", command=self.click_start)
        click_menu.add_separator()
        keyboard.add_hotkey('alt+x', self.click_stop)
        click_menu.add_command(label="停止", accelerator="Alt + X", command=self.click_stop)
        menubar.add_cascade(label="点赞", menu=click_menu)

        # 系统
        system_menu = Menu(menubar, tearoff=False)
        keyboard.add_hotkey('alt+e', self.confO.saveConf)
        system_menu.add_command(label="保存配置", accelerator="Alt + E", command=self.confO.saveConf)
        system_menu.add_separator()
        keyboard.add_hotkey('alt+n', self.change_bg)
        system_menu.add_command(label="调色板", accelerator="Alt + N", command=self.change_bg)
        keyboard.add_hotkey('esc', self.close)
        system_menu.add_command(label="退出", accelerator="ESC", command=self.close)
        menubar.add_cascade(label="工具", menu=system_menu)

        self.menu_comps.append(comment_menu)
        self.menu_comps.append(click_menu)
        self.menu_comps.append(system_menu)
        # 显示菜单
        self.config(menu=menubar)
        # 设置主题

    def apply_theme(self):
        rgb_str = self.cget('bg')
        for label in self.label_comps:
            label.configure(background=rgb_str)

        input_bg = '#%02x%02x%02x' % (min(max(0, int(rgb_str[1:3], 16) + 15), 255),
                                      min(max(0, int(rgb_str[3:5], 16)), 255),
                                      min(max(0, int(rgb_str[5:7], 16) + 15), 255))
        for label in self.input_comps:
            label.configure(background=input_bg)

        menu_bg = '#%02x%02x%02x' % (min(max(0, int(rgb_str[1:3], 16) - 50), 255),
                                     min(max(0, int(rgb_str[3:5], 16) - 20), 255),
                                     min(max(0, int(rgb_str[5:7], 16) - 50), 255))
        for menu in self.menu_comps:
            for index in range(menu.index("end") + 1):
                menu.entryconfigure(index, background=menu_bg)

    def change_bg(self):
        rgb, rgb_str = colorchooser.askcolor()
        self.config(background=rgb_str)
        self.confO.theme.set(rgb_str)
        self.apply_theme()




    def showInputList(self):
        window_bg = self.cget('bg')

        # 计算稍浅的颜色
        input_bg = '#%02x%02x%02x' % (max(0, int(window_bg[1:3], 16) + 15),
                                      max(0, int(window_bg[3:5], 16)),
                                      max(0, int(window_bg[5:7], 16) + 15))

        h, xg = 50, 10
        lw, iw, kw, bw = 100, 200, 200, 80
        # 列宽
        cw = lw + iw + xg + 10
        r = 0
        c = 0
        label1 = Label(anchor="w", text="点赞工具：Alt+D启动、Alt+x停止", fg="red", bg=self.cget('bg'))
        label1.place(width=kw * 2, height=h, x=c * cw + xg, y=r * h)

        r = 1
        c = 0
        label2 = Label(anchor="w", text="点赞次数：", bg=self.cget('bg'))
        label2.place(width=lw, height=h, x=c * cw + xg, y=r * h)
        entry2 = Entry(exportselection=True, textvariable=self.confO.clickTotal, bg=input_bg)
        entry2.place(width=iw, height=h, x=c * cw + lw + xg, y=r * h)
        c = 1
        label3 = Label(anchor="w", text="点赞间隔：", bg=self.cget('bg'))
        label3.place(width=lw, height=h, x=c * cw + xg, y=r * h)
        entry3 = Entry(exportselection=True, textvariable=self.confO.clickTm, bg=input_bg)
        entry3.place(width=iw, height=h, x=c * cw + lw + xg, y=r * h)
        r = 2
        c = 0
        label4 = Label(anchor="w", text="点赞坐标：", bg=self.cget('bg'))
        label4.place(width=lw, height=h, x=c * cw + xg, y=r * h)
        entry4 = Entry(exportselection=True, textvariable=self.confO.clickXY, bg=input_bg)
        entry4.place(width=iw, height=h, x=c * cw + lw + xg, y=r * h)

        r, c = 3, 0
        label5 = Label(anchor="w", text="输入工具：Alt+O/P/Q启动、Alt+y停止", fg="red", bg=self.cget('bg'))
        label5.place(width=kw * 2, height=h, x=c * cw + xg, y=r * h)
        r, c = 4, 0
        label6 = Label(anchor="w", text="输入内容：", bg=self.cget('bg'))
        label6.place(width=lw, height=h, x=c * cw + xg, y=r * h)
        entry6 = Entry(exportselection=True, textvariable=self.confO.inputText, bg=input_bg)
        entry6.place(width=iw * 2 + lw + xg * 2, height=h, x=c * cw + lw + xg, y=r * h)
        r, c = 5, 0
        label7 = Label(anchor="w", text="输入次数：", bg=self.cget('bg'))
        label7.place(width=lw, height=h, x=c * cw + xg, y=r * h)
        entry7 = Entry(exportselection=True, textvariable=self.confO.inputTotal, bg=input_bg)
        entry7.place(width=iw, height=h, x=c * cw + lw + xg, y=r * h)
        c = 1
        label8 = Label(anchor="w", text="提交间隔：", bg=self.cget('bg'))
        label8.place(width=lw, height=h, x=c * cw + xg, y=r * h)
        entry8 = Entry(exportselection=True, textvariable=self.confO.submitTm, bg=input_bg)
        entry8.place(width=iw, height=h, x=c * cw + lw + xg, y=r * h)
        r, c = 6, 0
        label9 = Label(anchor="w", text="输入坐标：", bg=self.cget('bg'))
        label9.place(width=lw, height=h, x=c * cw + xg, y=r * h)
        entry9 = Entry(exportselection=True, textvariable=self.confO.inputXY, bg=input_bg)
        entry9.place(width=iw, height=h, x=c * cw + lw + xg, y=r * h)
        c = 1
        label10 = Label(anchor="w", text="提交坐标：", bg=self.cget('bg'))
        label10.place(width=lw, height=h, x=c * cw + xg, y=r * h)
        entry10 = Entry(exportselection=True, textvariable=self.confO.submitXY, bg=input_bg)
        entry10.place(width=iw, height=h, x=c * cw + lw + xg, y=r * h)
        r, c = 7, 0
        label11 = Label(anchor="w", text="随机文本选项(按行随机)：", fg="blue", bg=self.cget('bg'))
        label11.place(width=kw * 2, height=h, x=c * cw + xg, y=r * h)
        r, c = 8, 0
        self.optionsText = Text(height=10, bg=input_bg)
        self.optionsText.place(width=(iw + lw + xg) * 2, height=h * 2 + 3, x=c * cw + xg, y=r * h)

        self.label_comps.append(label1)
        self.label_comps.append(label2)
        self.label_comps.append(label3)
        self.label_comps.append(label4)
        self.label_comps.append(label5)
        self.label_comps.append(label6)
        self.label_comps.append(label7)
        self.label_comps.append(label8)
        self.label_comps.append(label9)
        self.label_comps.append(label10)
        self.label_comps.append(label11)

        self.input_comps.append(entry2)
        self.input_comps.append(entry3)
        self.input_comps.append(entry4)
        self.input_comps.append(entry6)
        self.input_comps.append(entry7)
        self.input_comps.append(entry8)
        self.input_comps.append(entry9)
        self.input_comps.append(entry10)
        self.input_comps.append(self.optionsText)

        """ ---------------------------------------------------------------------- """

    def update_coordinate_show(self):
        def update_coordinate():
            while True:
                # 当前坐标
                point = pyautogui.position()
                tx, ty = point[0], point[1]
                # 当前窗口/组件句柄
                hwnd = win32gui.WindowFromPoint((tx, ty))

                self.title(self.all_title + " {}：(x={},y={})".format(hwnd, tx, ty))
                sleep(110 / 1000)

        t1 = threading.Thread(target=update_coordinate, name="t1")
        t1.start()

    ''' 点击操作 '''

    # 点击指定次数
    def click_count(self):
        ix, iy = self.confO.get_xy_click()
        # 非法坐标、非法状态
        if ix == 0 or iy == 0:
            return
        # 处理
        while True:
            num = int(self.confO.clickTotal.get())
            if self.run_type is not RunType.click or num <= 0:
                break

            # 点击
            pyautogui.click(x=ix, y=iy)

            self.confO.clickTotal.set(num - 1)
            sleep(int(self.confO.clickTm.get()) / 1000)

    def click_start(self):
        self.run_type = RunType.click
        threading.Thread(target=self.click_count).start()

    def click_stop(self):
        self.run_type = RunType.Stop

    ''' 输入操作 '''

    def input_count(self):

        ix, iy = self.confO.get_xy_input()
        sx, sy = self.confO.get_xy_input_submit()

        # 非法坐标、非法状态
        if ix == 0 or iy == 0:
            return
        # 处理
        while True:
            num = int(self.confO.inputTotal.get())
            if self.run_type not in (RunType.Input, RunType.InputRandom, RunType.InputSort) or num <= 0:
                break
            if self.run_type == RunType.InputSort:
                self.confO.inputText.set(self.randomO.sortNextText())
            elif self.run_type == RunType.InputRandom:
                self.confO.inputText.set(self.randomO.randomText())

            # 点击输入框
            pyautogui.click(x=ix, y=iy)
            sleep(0.01)

            # 输入文本
            keyboard.write(self.confO.inputText.get())
            sleep(0.01)
            pyautogui.click(x=ix, y=iy)

            # 提交
            if sx == 0 or sy == 0:
                # enter提交
                pyautogui.press('enter')
            else:
                # 按钮提交
                pyautogui.click(x=sx, y=sy)

            self.confO.inputTotal.set(num - 1)
            sleep(int(self.confO.submitTm.get()) / 1000)

    def input_start(self):
        if self.run_type not in (RunType.Input, RunType.InputRandom, RunType.InputSort):
            self.run_type = RunType.Input
            threading.Thread(target=self.input_count).start()

    def input_random_start(self):
        if self.run_type not in (RunType.Input, RunType.InputRandom, RunType.InputSort):
            self.run_type = RunType.InputRandom
            threading.Thread(target=self.input_count).start()

    def input_sort_start(self):
        if self.run_type not in (RunType.Input, RunType.InputRandom, RunType.InputSort):
            self.run_type = RunType.InputSort
            threading.Thread(target=self.input_count).start()

    def input_stop(self):
        self.run_type = RunType.Stop

    # 退出
    def close(self):
        # 保存配置
        self.confO.saveConf()
        # 保存文案
        self.randomO.saveByTextComp(app.optionsText)
        self.destroy()
        os._exit(0)  # 强行退出


if __name__ == '__main__':
    app = Main()
    app.protocol('WM_DELETE_WINDOW', app.close)
    app.mainloop()
